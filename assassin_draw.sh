#!/bin/bash

#
# Usage: (in empty directory)
#        touch player1 ... playern
#        assassin_draw.sh *
#        for i in *; do mv $i $i.txt; done
#
#        to remove suffices:
#
#        for i in *; do mv $i ${i%.txt}; done
#
# As a result name of a victim is written to player files
#

count=$#
# permutation represents circular graph
permutation=`shuf -i 1-$count`

# first victim is last in the permutation
victimn=`echo $permutation | cut -d " " -f $count`

for i in $permutation; do
	killer="${!i}"
	victim="${!victimn}"
	# next victim will be current killer, i.e. previous item in permutation
	victimn=$i
	echo $victim > $killer
done
